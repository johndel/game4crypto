# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180506121027) do

  create_table "admin_users", force: :cascade do |t|
    t.string   "username",        limit: 255, null: false
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255, null: false
    t.string   "profilepic",      limit: 255
    t.string   "password_digest", limit: 255
    t.string   "remember_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "global_infos", force: :cascade do |t|
    t.string   "wallet_balance", limit: 255, default: "0"
    t.string   "pool_balance",   limit: 255, default: "0"
    t.string   "total_promised", limit: 255, default: "0"
    t.string   "total_paid",     limit: 255, default: "0"
    t.string   "total_owed",     limit: 255, default: "0"
    t.boolean  "stop",                       default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "matches", force: :cascade do |t|
    t.integer  "uid",             limit: 8,     null: false
    t.integer  "user_id",         limit: 4
    t.string   "winner",          limit: 255
    t.string   "first_blood",     limit: 255
    t.datetime "started_at"
    t.string   "mode",            limit: 255
    t.string   "duration",        limit: 255
    t.string   "cluster",         limit: 255
    t.integer  "likes",           limit: 4
    t.integer  "dislikes",        limit: 4
    t.text     "towers_status",   limit: 65535
    t.text     "barracks_status", limit: 65535
    t.string   "match_type",      limit: 255
    t.boolean  "unpaid"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "matches", ["user_id"], name: "index_matches_on_user_id", using: :btree

  create_table "mining_sessions", force: :cascade do |t|
    t.integer  "user_id",        limit: 4,                 null: false
    t.integer  "state",          limit: 4,   default: 0
    t.integer  "hashrate_avg",   limit: 4
    t.integer  "seconds",        limit: 4,   default: 0
    t.integer  "count",          limit: 4,   default: 0
    t.datetime "starttime"
    t.datetime "endtime"
    t.string   "digibyte",       limit: 255, default: "0"
    t.integer  "profit_rate_id", limit: 4
    t.integer  "match_id",       limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "players", force: :cascade do |t|
    t.integer  "match_id",         limit: 4
    t.string   "uid",              limit: 255
    t.text     "hero",             limit: 65535
    t.integer  "level",            limit: 4
    t.integer  "kills",            limit: 4
    t.integer  "deaths",           limit: 4
    t.integer  "assists",          limit: 4
    t.integer  "last_hits",        limit: 4
    t.integer  "denies",           limit: 4
    t.integer  "gold",             limit: 4
    t.integer  "gpm",              limit: 4
    t.integer  "xpm",              limit: 4
    t.string   "status",           limit: 255
    t.integer  "gold_spent",       limit: 4
    t.integer  "hero_damage",      limit: 4
    t.integer  "tower_damage",     limit: 4
    t.integer  "hero_healing",     limit: 4
    t.text     "items",            limit: 65535
    t.integer  "slot",             limit: 4
    t.boolean  "radiant",                        null: false
    t.text     "additional_units", limit: 65535
    t.text     "ability_upgrades", limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "players", ["match_id"], name: "index_players_on_match_id", using: :btree
  add_index "players", ["radiant"], name: "index_players_on_radiant", using: :btree
  add_index "players", ["uid"], name: "index_players_on_uid", using: :btree

  create_table "profit_rates", force: :cascade do |t|
    t.string   "rate",       limit: 255, null: false
    t.string   "dgbcount",   limit: 255
    t.string   "hashrate",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "nickname",        limit: 255
    t.string   "uid",             limit: 255
    t.string   "miningid",        limit: 255
    t.string   "uid_32",          limit: 255
    t.string   "avatar_url",      limit: 255
    t.string   "profile_url",     limit: 255
    t.string   "credits",         limit: 255, default: "0"
    t.string   "total_credits",   limit: 255, default: "0"
    t.integer  "sessions",        limit: 4,   default: 0
    t.integer  "mom",             limit: 4,   default: 0
    t.string   "password_digest", limit: 255
    t.string   "remember_digest", limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "users", ["uid"], name: "index_users_on_uid", unique: true, using: :btree

  create_table "withdraws", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.string   "amount",     limit: 255, null: false
    t.string   "address",    limit: 255, null: false
    t.string   "txid",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "matches", "users"
  add_foreign_key "players", "matches"
end
