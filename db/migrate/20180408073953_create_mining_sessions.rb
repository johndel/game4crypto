class CreateMiningSessions < ActiveRecord::Migration
  def change
    create_table :mining_sessions do |t|
    	t.integer :user_id,null:false
    	t.integer :state,default:0  #0 is  mining, 1 is ended, 2 is invalid(less than allowed time)
    	#t.integer :reportedhashrate,default:0#in khps from windows miner
    	#t.integer :observedhashrate,default:0#in khps from pool
    	#t.integer :observed_avg_hashrate,default:0#MOST IMP ONE. CALCULATIONS WILL BE BASED OF THIS
    	t.integer :hashrate_avg
    	t.integer :seconds,default:0#seconds mined for 
    	t.integer :count,default:0#count of readings taken. observed_avg_hashrate is observedhashrate/count
      	t.datetime :starttime
        t.datetime :endtime#lastupdatedtime
    	t.string :digibyte,default:0#dbg earned in this session. Session has to be min 20 minute to earn dgb
    	t.integer :profit_rate_id#the profit object which was used
    	t.integer :match_id


      t.timestamps null: false
    end
  end
end
#time miner for is endtime-starttime


#There is a service which will run and querry all rows with state as 0. for all those, if endtime is
# less than 4 mins, then it will be marked as 1 and digibyte will be calculated