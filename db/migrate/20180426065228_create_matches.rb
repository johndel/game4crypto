class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :uid,limit:8, null: false
      t.references :user, index: true, foreign_key: true
      t.string :winner
      t.string :first_blood
      t.datetime :started_at
      t.string :mode
      t.string :duration
      t.string :cluster
      t.integer :likes
      t.integer :dislikes
      t.text :towers_status
      t.text :barracks_status
      t.string :match_type
      t.boolean :unpaid

      t.timestamps null: false
    end
  end
end
