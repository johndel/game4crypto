class CreateProfitRates < ActiveRecord::Migration
  def change
    create_table :profit_rates do |t|
    	t.string :rate,null:false#tells how much dollar u will earn for 0.1Ghps
    	t.string :dgbcount
    	t.string :hashrate#ideally is 0.1Ghps

      t.timestamps null: false
    end
  end
end
