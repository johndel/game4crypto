class CreateGlobalInfos < ActiveRecord::Migration
  def change
    create_table :global_infos do |t|
    	t.string :wallet_balance,default:0
    	t.string :pool_balance,default:0
    	t.string :total_promised,default:0
    	t.string :total_paid,default:0
    	t.string :total_owed,default:0
    	t.boolean :stop, default:false
    	

      t.timestamps null: false
    end
  end
end
#total_promised=total_paid+total_owed