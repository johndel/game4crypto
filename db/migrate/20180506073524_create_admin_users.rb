class CreateAdminUsers < ActiveRecord::Migration
  def change
    create_table :admin_users do |t|
        t.string :username , :null=>false, unique: true
        t.string :name 
        t.string :email, :null=>false, unique: true
        t.string 	:profilepic


        t.string 'password_digest'
        t.string 'remember_digest'#this is to maintain login session through reboots
        

      t.timestamps null: false
    end
  end
end
