class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nickname
      t.string :uid
      t.string :miningid#sha1 hash of uid
      t.string :uid_32
      t.string :avatar_url
      t.string :profile_url
      t.string :credits, default:0
      t.string :total_credits, default:0
      t.integer :sessions,default:0
      t.integer :mom,default:0#minutes of mining
		t.string 'password_digest'
		t.string 'remember_digest'#this is to maintain login session through reboots
        
        

      t.timestamps null: false
    end

    add_index :users, :uid, unique: true
  end
end
