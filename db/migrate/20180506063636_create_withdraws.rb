class CreateWithdraws < ActiveRecord::Migration
  def change
    create_table :withdraws do |t|
    	t.integer :user_id,null:false
    	t.string :amount, null:false
    	t.string :address, null:false
    	t.string :txid
      t.timestamps null: false
    end
  end
end
