module ProfilesHelper
    POSTS_PERPAGE=20
    POST_COMMENT_PERPAGE=10
        def get_updates 
            @updates={}
            page=params[:page]
            @articles=get_articles page, false

            userfailure=my_issues

            @new_groups=get_new_groups current_user, userfailure,page

            article_authors=@articles.map{|a| a.user_id}
            group_creators=@new_groups.map{|a| a.started_by}
            alluser_ids=article_authors+group_creators
            #all_users=User.where("id in (?)",alluser_ids)
            all_users=CacheUsers.from_ids(alluser_ids)
            @map_id_to_all_users={}
            all_users.map do |p|
                @map_id_to_all_users[p.id]=p
            end


            @total_count=@articles.count+@new_groups.count
        end

        def get_ndigitrandom n
            num=Random.new.rand((10**(n - 1))..(10**n))
            return num
        end



        def getchat_name anon,user
            id=get_ndigitrandom 10
            if anon
                @chat_user_id=""
                @chat_username=""
                @chat_user_id_anon="anon_"+id.to_s
                @chat_username_anon="Anonymous"
            else
                #return his actual info here
                @chat_user_id=user.id
                @chat_username=user.username
                @chat_user_id_anon="anon_"+id.to_s
                @chat_username_anon="Anonymous"

            end

        end 

        #The channels where those people push updates for their followers
        #If i need to get their feed, i need to subscriube to their channel
        def get_notification_channels
            #Subscribe to followers channel of all bussies who we have followed
            @following=Buddy.where(mentee_id: current_user.id, following:true).pluck(:mentor_id)
        end

        def get_room userid
            return "personal_"+userid.to_s
        end

        #This is the channel where all my sockets listen to
        #if someone wants to tell me something they shud put it here. this is like my mailbox
        def get_myroom
            return get_room current_user.id
        end
        #the rooms that he needs to register to
        def get_rooms
            isbuddy=get_current_buddystatus 
            @rooms_tojoin={}
            #structur is {"anonymous": true, groups:[], follow:[] , incoming=[]} 
            #Both follow and incoming gives upodates in which I hav to act on but the difference between follow and incoming is that
            #folow is a broadcast channel for others who send updates to people interested
            #incoming is directed to me and I do hav to act on it
            if isbuddy
                @rooms_tojoin["anonymous"]=true
            end

            #All follow channels
            get_notification_channels
            @rooms_tojoin["followers"]=[]

            @following && @following.each do |mentor_id|
                @rooms_tojoin["followers"].push( "followers_"+mentor_id.to_s)
            end

            #All incoming channels
            myroom=get_myroom
            @rooms_tojoin["incoming"]=[]

            @rooms_tojoin["incoming"].push( myroom)


            #All group channels
            #Todo

        end
        def summarise_updates_for_public_timeline
			@updates=MiningSession.where(user_id: current_user.id,state:1).order("id desc").page(params[:page]).per(POSTS_PERPAGE)

        	@running_session=MiningSession.where(user_id: current_user.id,state:0).limit(1)
        	if @running_session.count>=1
        		@running_session=@running_session[0]
        	else
        		@running_session=nil
        	end
		end

        def get_basic_profileinfo
            find_groups_joined(current_user) #inside therapyhelper
            get_rooms#rooms in nodejs channels that he needs to register to

            getchat_name false , current_user
        end        



        def get_basic_guestinfo
            @user=User.new#well show him signup page
            getchat_name true, nil
        end

        def get_basic_profileinfo_others user_id
            @person=CacheUsers.from_id user_id 
            #debugger
            @user_info=@person.user_info
        end

        def add_gunnerspace
            gunner=User.first
            (gunner && gunner.username=="gunnerspace") ?  gunner.id : nil
        end

    
        def summarise_updates_for_public_timeline2#Not Implemented Yet
		    #The above function shows everything in reverse chronological order
            #We need to find a way by which impoprtant tweets are showed first.
            #But important tweets alredy seen shud not be shown before less important ones not seen
            #So the logic is as follows
            #Let x be the post_id which he last saw.
            #And say everytime a post is being liked, or commented on we modify the post to save likecount and commentcount
            #So we take every post where id> x in order of modified timedesc (not created time), limit 30
            #So every post which was not seen will be placed in the order that it was most active
            #if we get less than 30 unseen posts, we need to extract some seen posts now.
            #Now get all posts where post_id<x order of modified timedesc limit 30 . So we will 
		end
            
            #To only get things posted by me
        def  summarise_updates_get_posts_foruser userid, failure_id="All"
            
            @mystory=MyStory.find_by(user_id: userid)
            @issues=UserFailure.where(user_id: userid)
            #@issueEventArray=[]
            #@issues && @issues.each do |issue|
                #myevents=Event.where(user_id: user.id,user_failure_id: issue.id)
                #issuename=Failure.failure_types[ issue.failure_id]
                #hash={issue: issuename, events: myevents}
                #@issueEventArray.push hash
            #end

            if failure_id=="All" || failure_id==nil then 
                @myevents=Event.where(user_id: userid)
            else
                @myevents=Event.where(user_id: userid,failure_id: failure_id)
            end

            @map_id_to_all_users={} unless @map_id_to_all_users
            @map_id_to_all_users[current_user.id]=current_user



            #debugger
        end

    
        def  getfollowings user
            idlist = getfollowings_id user
            @following=User.where("id in (?)",idlist)

        end 
        def getfollowings_id user
            @following_id=Following.where(follower_id:user.id,follow:true).pluck(:followee_id)#for folowing
            return @following_id

        end
        def  getfollowers user
            @followers_id=Following.where(followee_id: user.id,follow:true).pluck(:follower_id)#for folowing
            @followers=User.where("id in (?)",@followers_id)


        end
        def like_post_foruser post_id,liketype
            #check if alredy some action has happened
                        #debugger            

            @postlike=PostLike.find_by(user_id:current_user.id, post_id: post_id)
            if @postlike 
                if (@postlike.liketype==0 && liketype=="Like") || (@postlike.liketype==1 && liketype=="UnLike")
                    return;#we are alredy in the requestted state. Nothing to do here
                end
                if liketype=="Like"
                    @postlike.update_attribute("liketype" ,0)
                    Post.increment_counter(:likes, post_id ) 

                else
                    @postlike.update_attribute("liketype", 1)
                    Post.decrement_counter(:likes, post_id ) 

                    @newlike_type="Like"

                end
            else
                @postlike=PostLike.new(user_id:current_user.id, post_id: post_id,liketype:0)
                @postlike.save

                Post.increment_counter(:likes, post_id ) 
            end
        end

        def like_post_comment_foruser comment_id,liketype
            #check if alredy some action has happened

            @post_comment_like=PostCommentLike.find_by(user_id:current_user.id, post_comment_id: comment_id)
            if @post_comment_like 
                if (@post_comment_like.liketype==0 && liketype=="Like") || (@post_comment_like.liketype==1 && liketype=="UnLike")
                    return;#we are alredy in the requestted state. Nothing to do here
                end
                if liketype=="Like"
                    @post_comment_like.update_attribute("liketype" ,0)
                    PostComment.increment_counter(:likes, comment_id ) 

                else
                    @post_comment_like.update_attribute("liketype", 1)
                    PostComment.decrement_counter(:likes, comment_id ) 

                    @newlike_type="Like"

                end
            else
                @post_comment_like=PostCommentLike.new(user_id:current_user.id, post_comment_id: comment_id,liketype:0)
                @post_comment_like.save

                PostComment.increment_counter(:likes, comment_id ) 
            end
        end

        def upload_base_64_image value
            value.gsub!('data:image/jpeg;base64,', "")

            #File.open("/public/myimage.png", 'w') { |file| file.write(realdata) }
            #File.write("myimage.png", value)
            tempfile=Tempfile.new "myimage.png"
            File.open(tempfile.path, 'wb') do|f|
                f.write(Base64.decode64(value))
            end

            album=Album.find_by(user_id: current_user.id,  albumname:"Formations")
            if album.nil?
                album=Album.new(user_id: current_user.id, albumname: "Formations", caption: "This is a default album containing pictures from your Formations created")
                album.save
            end

            hash={tempfile: tempfile, filename: "formation",type: "image/jpeg" , head: ""}
            pic=ActionDispatch::Http::UploadedFile.new hash


            @album_pic=AlbumPicture.new(user_id: current_user.id,album_id:album.id, picture:pic)
            @album_pic.save

            @image_url= @album_pic.picture.url
        end
        def upload_image_or_formation image , image_url, formation_image
            text=""
            if image 
                album=Album.find_by(user_id: current_user.id,  albumname:"Posts")
                if album.nil?
                    album=Album.new(user_id: current_user.id, albumname: "Posts", caption: "This is a default album containing pictures from your posts")
                    album.save
                end
                @album_pic=AlbumPicture.new(user_id: current_user.id,album_id:album.id, picture:image)
                @album_pic.save

                @seturl=true
                @image_url=@album_pic.picture.url
                text='![Image]('+@image_url+' "Image title")'+"\n\r"
            elsif image_url && image_url.length > 0

                @image_url=image_url
                text='![Image]('+@image_url+' "Image title")'+"\n\r"
            elsif formation_image && formation_image.length> 0
                upload_base_64_image formation_image
                text='![Image]('+@image_url+' "Image title")'+"\n\r"
            end
            return text
        end
        def santize_taggedusers comment#Todo: complete this
            email_arr=[]

            #tagged_arr=tagged_ids.split ","
            #tagged_arr && tagged_arr.each do |user_id|
                #user=User.find_by(id:user_id)
                #email_arr.push("@"+user.username) if user
                ##check if this email/username is present in the comment. if yes then record its index and length. Then remove from comment
                ##while displaying well replace it with his link
                ##irrespective of the order in username_tagged the one in comment could be anywhere. so its imp that we record its startindex,endindex in username_tagged as well
            #end

            #converted=comment.gsub(/@(\w+)/, "<a href='/\\1'>\\1</a>")
            converted=comment.gsub(/@(\w+)/) do |mention| 
                mention.slice! 0 if (mention && mention.length>0)
                user=User.find_by(username:mention)

                #mention="<a href='/users/#{user.id}'>#{user.username}</a>"
                if user
                    #mention= "[#{user.username}](/show_user_profile?id=#{user.id})" 
                    mention= "[#{user.username}](/mention#{user.id})" 
                else
                    mention="@"+mention
                end
            end
            return converted



        end     
    
    def redcarpet_process_hashtag id ,text
        url= "<span class='hashtag' id=#{id}>#{text}</span>".html_safe
        
    end    
    def redcarpet_process_mention id ,text
        url= "<span class='mention' id=#{id}>#{text}</span>".html_safe
    end
    def santize_hashtags comment#Todo: complete this
        email_arr=[]
        @tags_to_save=[]
        @tags_to_create=[]
        seen_tags=[]
        count=0#for keeping track of how many tags we added. we wont add more than 10
        converted=comment.gsub(/#(\w+)/) do |tag| 
            tag_present=false
            tag.slice! 0 if (tag && tag.length>0)
            count+=1
            if count>10 || seen_tags.include?(tag)
                "#"+tag
            else
                
                db_tag=HashTag.find_by(hashtag_title:tag)
                if db_tag
                    tag_present=true
                    HashTag.increment_counter "count",db_tag.id
                    if @tags_to_save && @tags_to_save.length<3
                        h={name: db_tag.hashtag_title,id: db_tag.id}
                        @tags_to_save.push h
                    end
                    @tags_to_create.push db_tag.id
                    seen_tags.push tag
                else
                    #create a new tag
                    db_tag=HashTag.new(hashtag_title:tag,count:1)
                    tag_present=db_tag.save
                    if tag_present && @tags_to_save && @tags_to_save.length<3
                        h={name: db_tag.hashtag_title,id: db_tag.id}
                        @tags_to_save.push h
                    end
                    if tag_present#if sucesfuly created
                        @tags_to_create.push db_tag.id 
                        seen_tags.push h
                    end

                end

                #mention="<a href='/users/#{user.id}'>#{user.username}</a>"
                if tag_present
                    tag="[#{'#'+tag}](/hashtag#{db_tag.id})" 
                else
                    tag="#"+tag
                end
            end

        end
        puts "hi"
        return converted



    end

    def get_comment_list_for_post post_id,page
        @comments=PostComment.where(post_id: post_id).order("id DESC").page(page).per(POST_COMMENT_PERPAGE)
        userlist=@comments.pluck(:user_id)
        comment_ids=@comments.pluck(:id)
        userlist.uniq!
        preload_users userlist

        @already_liked_comment__ids=PostCommentLike.where("post_comment_id in (?)",comment_ids).where( user_id:current_user.id,liketype:0).
                                                                                    pluck(:post_comment_id)
        @comments=@comments.reverse
    end

    #the function which loads all users from the list
    #supposed to be used when no redis caching of users 
    def preload_users userlist
        users=User.where("id in (?)",userlist)
        @user_hash=[]
        users.each do |user|
            @user_hash[user.id]=user
        end

    end
    def get_user_from_hash id
        #Maybe this needs to be static so as to survive a request response cycle??
        @user_hash=[] if @user_hash.nil?
        user=@user_hash[id]
        if user.nil?
            #Slowly populate the cache
            user=User.find_by(id: id)
            @user_hash[id]=user
        end
        return user

    end
    
    def get_failure failure_id
        failure=nil
        if @failure_hash.nil?
            @failure_hash={}
            allfailures=Failure.all
            allfailures.each do |failure|
                @failure_hash[failure.id]=failure
            end
        end
        begin
            failure= @failure_hash[failure_id]
        rescue
            failure=nil

        end
        return failure
    end
    #akes userid returns array of failure_ids
    def user_failurelist userid
        @userfailure_hash=[] if @userfailure_hash.nil?
        userfailure=@userfailure_hash[userid]
        if userfailure.nil?
            #Slowly populate the cache
            userfailure=UserFailure.where(user_id: userid)#.pluck(:failure_id)

            @userfailure_hash[userid]=userfailure
        end
        return userfailure 
    end

    def my_issues
        issues=UserFailure.where(user_id:current_user.id).pluck(:failure_id)
        return issues

    end

end
