module AdminUsersHelper
	  #Logs a adminuser in, we only set cookie. currentuser var is set only when its function  is called
    def log_in_admin (admin)
		#Session is a rails method which saves something called as userid in a cookie
		session[:admin_user_name]=admin.username

	end

	#gets the currently logged in user
	def current_admin
		
		#if there is a session object, use it. otherwise look at the cookie
		if (admin_user_name = session[:admin_user_name])#This is not comparing with user_name. it is assigning to it
			#if currentuser has someone it returns him. if it is the first call it will be empty in which case
			#we check the cookie and get the id of the guy who is logged in
			#@current_user ||= AdminUser.find_by(username: user_name)
            @current_admin ||= AdminUser.find_by(username: admin_user_name)

		#if there was no session id it means either nothing is saved or browser was closed. check for cookie
		elsif (admin_user_name = cookies.signed[:admin_user_name])	#The signed function will decrypt the id and give
			#find the user in db whose id is stored in cookie
            admin = AdminUser.find_by(username: admin_user_name)	

			 #The authenticated? method is defined in AdminUser model and authenticates this against what is saved in db using remember token
			 #So that way it comes to know if the id belongs to the same guy who was logged in prevoiusly
			if admin && admin.authenticated?(cookies[:admin_remember_token])
				#log in the user
				log_in_admin admin
				#update the variable so that next time it doesnt have to execute this elseif statement
				@current_admin = admin
			end
		end
	end
	
	#calls the current_user method and tells if anyone is logged in at all
	def admin_logged_in?
		#if currentuser.nil returns true then logedin is false
		!current_admin.nil?
	end

	#Logs a admin user out
	def log_out_admin
		#delete the cookie
		forget_admin(current_admin)
		#delete the sessionid
  		session[:admin_user_name]=nil
  		#then currentuser is deleted
		@current_admin=nil
	end

	# Remembers a user in a persistent session.
	def remember_admin(admin)
		#calls the remember method in AdminUser model
		admin.remember
		#cookies method helps to do permanent sessions. This is actually a syntactic sugar for a bigger thing. 
		cookies.permanent.signed[:admin_user_name] = admin.username
		#save the remembertoken as well. this dosent need to be encrypted
		cookies.permanent[:admin_remember_token] = admin.remember_token
	end

	# Forgets a persistent session.
	def forget_admin(admin)
		#call the model.forget api to delete the remember_token from user
		admin.forget
		#remove cookie from browser
		cookies[:admin_user_name]=nil
		#remove remembertoken from browser
		cookies[:admin_remember_token]=nil
	end
    
    

	
end
