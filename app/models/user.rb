require 'digest/sha1'

class User < ActiveRecord::Base
	has_secure_password
  has_many :matches


	#This accessor will have a unique string and the database will have a digest version of this string for 
	#the user who is logged in currently
	attr_accessor :remember_token

    def get_profile_pic
		if avatar_url && avatar_url.length>0 then 
			return avatar_url
		end
		return "user_no_image5.jpg"
	end
    def get_profile_pic_url
		if avatar_url && avatar_url.length>0 then 
			return avatar_url.url
		end
        return nil
	end

	# Returns the hash digest of the given string.
	def digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random string token.
	def new_token
		SecureRandom.urlsafe_base64
	end

	#Saves the remember token in db
	def remember
		#This creates and assignes a new string token into the accessor
		self.remember_token = new_token
		#We can call update_attribute directly becoz we are alredy in the Model object
		#update_attribute can change db itemw for users without passing in a password
		update_attribute(:remember_digest, digest(remember_token))
	end

	def get_remembertoken
		return self.remember_token
	end

	# Returns true if the given token matches the digest.
	def authenticated?(remember_token)
		#if logged in from multiple browsers, and logged out from 1, this cud be null
		return false if remember_digest.nil?
		#hashes the token and compares it with digest
		BCrypt::Password.new(remember_digest).is_password?(remember_token)# you can use == also in place of is_password
	end

	# Forgets a user.
	def forget
		update_attribute(:remember_digest, nil)
	end

	def username
		return nickname
	end

	def add_credits dgb
		debugger
		
		c=credits.to_f
		c+=dgb.to_f
		credits=c.to_s
		debugger
		credits=c.to_s

	end

	

	  def won?(match)
	    player = find_self_in(match)
	    (player.radiant? && match.winner == 'Radiant') || (!player.radiant? && match.winner == 'Dire') if player
	  end

	  def played_for_in(match)
	    user=find_self_in(match)
	    user.hero if user
	  end

	 private

	  def find_self_in(match)

	    match.players.find_by(uid: uid_32.to_i)
	  end




  class << self

	  def parse_buildings(arr)
	    arr.keep_if {|k, v| v }.keys
	  end    

      def parse_duration(d)
	    hr = (d / 3600).floor
	    min = ((d - (hr * 3600)) / 60).floor
	    sec = (d - (hr * 3600) - (min * 60)).floor

	    hr = '0' + hr.to_s if hr.to_i < 10
	    min = '0' + min.to_s if min.to_i < 10
	    sec = '0' + sec.to_s if sec.to_i < 10

	    hr.to_s + ':' + min.to_s + ':' + sec.to_s
	  end 

  	def readmatches(user_id,count)
  		user=User.find_by(id:user_id)

  		return unless user

  		users_matches=user.matches
  		match_ids={}
  		users_matches && users_matches.each do |umatch|
			match_ids[umatch.uid]=true
  		end


	    matches_arr = Dota.api.matches(player_id: user.uid_32)
	    #matches_arr = Dota.api.matches(player_id: user.uid_32, limit: count)
	    if matches_arr && matches_arr.any?
	      matches_arr.each do |match|
			  next if match_ids[match.id]#if this match is already added then ignore
	          match_info = nil
	          i=0
	          loop do
	            # Sometimes Steam API returns nothing in response
	            match_info = Dota.api.matches(match.id)
	            i+=1
	            break if ((match_info && match_info.id) || i==4)#try 5 times
	            sleep(2)
	          end
	          if match_info
	            new_match = user.matches.create({
	                                            uid: match.id,
	                                            winner: match_info.winner.to_s.titleize,
	                                            first_blood: parse_duration(match_info.first_blood),
	                                            started_at: match_info.starts_at,
	                                            mode: match_info.mode,
	                                            cluster: match_info.cluster,
	                                            duration: parse_duration(match_info.duration),
	                                            match_type: match_info.type,
	                                            likes: match_info.positive_votes,
	                                            dislikes: match_info.negative_votes,
	                                            towers_status: {
	                                              radiant: parse_buildings(match_info.radiant.tower_status),
	                                              dire: parse_buildings(match_info.dire.tower_status)
	                                            },
	                                            barracks_status: {
	                                              radiant: parse_buildings(match_info.radiant.barracks_status),
	                                              dire: parse_buildings(match_info.dire.barracks_status)
	                                            },
	                                            unpaid:true
	                                          })
	            new_match.load_players(match_info.radiant, match_info.dire)

	          end
	        #end
	      end
	    end
	  end

	    def create_from_omniauth(auth)
			begin

				info = auth['info']
				# Convert from 64-bit to 32-bit
				#user = find_or_initialize_by(uid: (auth['uid'].to_i - 76561197960265728).to_s)
				uid_32=(auth['uid'].to_i - 76561197960265728).to_s

				loaddata=false
				user=find_by(uid: auth['uid'])
				loaddata=true unless user

				user = find_or_initialize_by(uid: auth['uid'])
				user.nickname = info['nickname']
				miningid=Digest::SHA1.hexdigest auth['uid']

				user.miningid=miningid[0..9]
				user.uid_32 = uid_32
				user.password=auth['uid']
				user.avatar_url = info['image']
				user.profile_url = info['urls']['Profile']
				user.save!
				if loaddata
					User.delay.readmatches(user.id,20)#asynch
				end
				user
			rescue Exception=> ex

			end

	    end
	  end
end
