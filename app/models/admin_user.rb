class AdminUser < ActiveRecord::Base
	  before_save{self.email=email.downcase}
	validates :username, presence:  true, length:{maximum:255},uniqueness: {case_sensitive:false}
	validates :email, presence: true, length:{maximum:255}, uniqueness: {case_sensitive:false}#Todo: put regex validation on this as shown in lesson 6
	has_secure_password
    	validates :password, length:{minimum:8}
	attr_accessor :remember_token
	# Returns the hash digest of the given string.
	def digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random string token.
	def new_token
		SecureRandom.urlsafe_base64
	end

	#Saves the remember token in db
	def remember
		#This creates and assignes a new string token into the accessor
		self.remember_token = new_token
		#We can call update_attribute directly becoz we are alredy in the Model object
		#update_attribute can change db itemw for users without passing in a password
		update_attribute(:remember_digest, digest(remember_token))
	end

	# Returns true if the given token matches the digest.
	def authenticated?(remember_token)
		#if logged in from multiple browsers, and logged out from 1, this cud be null
		return false if remember_digest.nil?
		#hashes the token and compares it with digest
		BCrypt::Password.new(remember_digest).is_password?(remember_token)# you can use == also in place of is_password
	end

	# Forgets a user.
	def forget
		update_attribute(:remember_digest, nil)
	end
    
    def get_profile_pic
        if profilepic && profilepic.length>0 then 
			return profilepic
		end
		return "user_no_image5.jpg"
	end
end
