class Match < ActiveRecord::Base
  belongs_to :user
  has_many :players, dependent: :delete_all

  serialize :towers_status
  serialize :barracks_status

  def load_players(radiant, dire)
    pair = {radiant: radiant, dire: dire}
    pair.each_pair do |k, value|
      value.players.each do |player|

        begin
	        items=parse_items(player.items)
	    rescue
	    	items=nil
	    end    

	    begin
	        ability_upgrades= player.ability_upgrades.map { |ability_upgrade| 
	        					{id: ability_upgrade.ability.id,
                                                   name: ability_upgrade.ability.name,
                                                   image: ability_upgrade.ability.image_url(:hp1),
                                                   level: ability_upgrade.level,
                                                   time: parse_duration(ability_upgrade.time)}
                              }.sort_by {|ability_upgrade| ability_upgrade[:level]}
	    rescue
	    	ability_upgrades=nil
	    end
        self.players.create({
                              uid: player.id,
                              items: items,
                              hero: {id: player.hero.id,
                                     name: player.hero.name,
                                     image: player.hero.image_url(:sb)
                              },
                              ability_upgrades: ability_upgrades,
                              additional_units: player.additional_units.map {
                                |unit| {name: unit.name,
                                        items: parse_items(unit.items)}
                              },
                              level: player.level,
                              kills: player.kills,
                              deaths: player.deaths,
                              assists: player.assists,
                              last_hits: player.last_hits,
                              denies: player.denies,
                              gold: player.gold,
                              gpm: player.gpm,
                              xpm: player.xpm,
                              status: player.status,
                              gold_spent: player.gold_spent,
                              hero_damage: player.hero_damage,
                              tower_damage: player.tower_damage,
                              hero_healing: player.hero_healing,
                              slot: player.slot,
                              radiant: k == :radiant
                            })
      end
    end
  end

  private

  def parse_items(items)
    items.delete_if {
      |item| item.name == "Empty"
    }.map  {
      |item| {id: item.id, name: item.name, image: item.image_url}
    }
  end
  def parse_duration(d)
    hr = (d / 3600).floor
    min = ((d - (hr * 3600)) / 60).floor
    sec = (d - (hr * 3600) - (min * 60)).floor

    hr = '0' + hr.to_s if hr.to_i < 10
    min = '0' + min.to_s if min.to_i < 10
    sec = '0' + sec.to_s if sec.to_i < 10

    hr.to_s + ':' + min.to_s + ':' + sec.to_s
  end 
end
