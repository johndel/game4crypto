class AdminUsersController < ApplicationController
	include UsersHelper
	include AdminUsersHelper
	include ProfilesHelper
    layout 'site_admins_layout'
    USRES_PERPAGE=20
	#serves the POST op for  login
  def login
    #checking for recaptch
    #captcha=params["g-recaptcha-response"]
    ip=""
    #if Rails.env.production?
      #ip=request.remote_ip
    #else
      #ip=Net::HTTP.get(URI.parse('http://checkip.amazonaws.com/')).squish
    #end
    #secret ="6Ld7FVgUAAAAAM5xW4ohkhVVKrKGv820AeVyeEQB"
      #<div class="g-recaptcha" data-sitekey="6Ld7FVgUAAAAAHNXfBfmNh-mHSlELwkz0DCE8FdW"></div>


    #link="https://www.google.com/recaptcha/api/siteverify?secret="+secret+"&response="+captcha+"&remoteip="+ip;
    #a=Faraday.post link

    admin=AdminUser.find_by(username: params[:session][:username])
  	if(admin && admin.authenticate(params[:session][:password]))
  		log_in_admin admin
  		remember_admin admin 
  		redirect_to admin
  	else
  		if !admin 
  			flash[:danger]="Invalid username or password"
  		else
  			flash[:danger]="Invalid password"
  		end
        redirect_to admin_path
  	end
  end

  #Shows the profile for a user after logging in 
  def show

    @site_admin=AdminUser.find_by(id: params[:id])
    @global_info=GlobalInfo.last

    if @site_admin.nil? || @site_admin!=current_admin then
  		redirect_to site_admins_path
  	else
		@users=User.all.page(params[:page]).per(USRES_PERPAGE)

    read_charts
  	end
  end

  def users_next
		@users=User.all.page(params[:page]).per(USRES_PERPAGE)

  end

  #called when admins and recruiters logout
  def destroy
  	#call logout method in admin_helper but only if we r loggedin
  	log_out_admin if admin_logged_in?
  	#redirect him to homepage
  	redirect_to admin_path
  end


  def new
    if current_admin
      redirect_to current_admin
    end
  end

  private

    def read_charts
#      reports=GlobalInfo.all.order("id DESC")
      reports=GlobalInfo.all.limit(50).order("id DESC")
      @array=[]
      reports.each do|item|

        val=item.wallet_balance.to_f+item.pool_balance.to_f - item.total_owed.to_f
        entry=[item.id,val.round(2)]
        @array.push entry
      end

    end
end
