class ApplicationController < ActionController::Base
      include UsersHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
    
    before_action :is_logged_in
    #rescue_from NoMethodError, with: :record_not_found

    def raise_not_found!
         #debugger
      record_not_found
    end
 
  private
 
    def record_not_found
      render plain: "404 Not Found", status: 404
    end
    def is_logged_in
        get_current_user#This will set the @current_user variable
    end
end
