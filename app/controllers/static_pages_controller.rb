class StaticPagesController < ApplicationController
  include UsersHelper
  def home
      if logged_in?
          redirect_to current_user
      end
  end

  def help
  end
    
  def faq
  end

  def about
  end

  def contact
  end
end
