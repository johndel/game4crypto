class ProfilesController < ApplicationController
    POSTS_PERPAGE=20

	def past_games
		@level_1_tab="profile_tab"
		@level_2_tab="past_games"
    	@matches = current_user.matches.order('started_at DESC').page(params[:page]).per(POSTS_PERPAGE) if current_user

	end		

	def how_it_works
		@level_1_tab="profile_tab"
		@level_2_tab="how_it_works"

	end	
	#dig.conf file settings
#https://cryptocurrencytalk.com/topic/3137-digibyte-dgb-information/

#https://bitcoin.stackexchange.com/questions/58453/how-to-connect-to-bitcoind-from-ruby-rpc-client

	def withdraw
		@level_1_tab="profile_tab"
		@level_2_tab="withdraw"
		@withdrawals=Withdraw.where(user_id:current_user.id).order("id DESC");
	end

	def withdraw_dgb
		@level_1_tab="profile_tab"
		@level_2_tab="withdraw"


		error="Something went wrong!"
		withdraw=nil
		begin
			amount=params[:data][:amount].to_f
			address=params[:data][:address]
			if current_user 
				unless Withdraw.validate_address(address)
					error="Incorrect address!" 
					throw
				end		
				if  current_user.credits.to_f<amount
					error="Insufficient Balance!" 
					throw
				end
				unless amount > 10
					error="Amount should be greater than 10!" 
					throw
				end			
				amount_actual=amount- (0.01*amount)
				h = DigiByteRPC.new('http://game:Acidance69@127.0.0.1:12038')
				#p h.getbalance
				txid=""
				txid=h.sendtoaddress address, amount_actual
				if txid 
					balance=current_user.credits.to_f-amount
					current_user.credits=balance.to_s
					current_user.save
					withdraw=Withdraw.new(user_id:current_user.id, amount: amount_actual.to_s,address: address,txid: txid)
					withdraw.save

					#reduce from global object
					global_info=GlobalInfo.last
					total_paid=global_info.total_paid.to_f
					total_owed    =global_info.total_owed.to_f
					total_paid+=amount
					total_owed-=amount

					global_info.total_paid=total_paid.to_s
					global_info.total_owed=total_owed.to_s
					global_info.save
				end
				render :json => {error:false,message:"Withdrawal successful Txid: "+txid}

			end
		rescue Exception => ex
			render :json => {error:true,message:error}

		end

	end

	def match_info
		@level_1_tab="match_info"
		@level_2_tab="match_info"
	    @match = Match.includes(:players).find_by(id: params[:id])
    	@players = @match.players.order('slot ASC').group_by(&:radiant)
	end
end

class DigiByteRPC
  def initialize(service_url)
    @uri = URI.parse(service_url)
  end

  def method_missing(name, *args)
    post_body = { 'method' => name, 'params' => args, 'id' => 'jsonrpc' }.to_json
    resp = JSON.parse( http_post_request(post_body) )
    raise JSONRPCError, resp['error'] if resp['error']
    resp['result']
  end

  def http_post_request(post_body)
    http    = Net::HTTP.new(@uri.host, @uri.port)
    request = Net::HTTP::Post.new(@uri.request_uri)
    request.basic_auth @uri.user, @uri.password
    request.content_type = 'application/json'
    request.body = post_body
    http.request(request).body
  end

  class JSONRPCError < RuntimeError; end
end
