class UsersController < ApplicationController
  include UsersHelper
	include ProfilesHelper

PAGINATION_PERPAGE=10
    
  #Shows the page to login and signup
  def new
    #When login fails, it comes here
      @user=User.new
  end
    
  skip_before_filter :verify_authenticity_token, only: :create


#Shows the profile for a user after logging in 
  def show
  	#if a request comes in to show a users profile page it comes here. From the id get the user
  	@user=User.find_by(id: params[:id])
  	#Show the page only if this user is the currently logged in user
  	if @user.nil? || @user!=current_user then
  		#if the user dosent exist or if he is not currently logged in we redirect him  to homepage
  		#redirect_to home_path
        @user=User.new

  	end
    @level_1_tab="profile_tab"    
    if current_user
        #if @user_info.stage==0
        #    @level_1_tab="welcome_tab"
        #end
            #@level_2_tab="profile_tab" 
        summarise_updates_for_public_timeline 
    end
    


            
    respond_to do |format|
        format.js do 
            render :file => "discussions/ajaxresponse/discussions.js.erb"
        end
        format.html do 
            render "users/show"
        end
    end
  end
      


      #serves the POST op for  sign up andd HOME PAGE after login
  def create
  	#Create a model object using the input from the form. We cant use the params directly becoz of safety reasons.
  	#So we call a function which explicitely permits the operation
    begin
  		@user=User.create_from_omniauth(request.env['omniauth.auth'])
    rescue
		flash[:error] = "Can't authorize you..."
    else
  		#if the signup is succeeded then we redirect him to homepage
  		#Commenting below login code becoz we dont want to login user just after signup. We will send a mail and user will have to activate his acciunt
  		log_in @user
      	remember @user 

      	session[:user_id] = @user.id
      	flash[:success] = "Welcome, #{@user.nickname}!"
        
        #Todo: do not log him in yet. make him activate his account first
  		redirect_to @user
  	end

  end

#NOT CALLED
  def login
	#find the user by username. here session is the hash inside params coz that is what we specified in the form_for of login
  	user=User.find_by(username: params[:session][:username])
  	#if user is not admin check him in the user model
  	if user.nil?
  		user=User.find_by(username: params[:session][:username])
  	end
  	#if the user is present as well as password matches. authenticate api is provided by has_secure_password
  	if(user && user.authenticate(params[:session][:password]))
  		#call the log_in method inside admin_users_helper
  		log_in user
  		#this function will save id and token in browser and hashed token in db
  		remember user 
  		#redirect to profile page. Note again that modelobjectname serves the purpose of routing to appropriate page
  		redirect_to user
  	else
  		#in case of unsuceesful login 
  		if !user 
  			#if there was no user then username was also wrong
  			flash[:danger]="Invalid username or password"
  		else
  			#user was present in which case only password mismatched
  			flash[:danger]="Invalid password"
  		end
      #I dont know right now which is the best page to redirect hi to. but taking hi to landing page now
      #redirect_to user_signup_path
      redirect_to root_path
  	end
  end

  #called when admins and recruiters logout
  def destroy
  	#call logout method in admin_helper but only if we r loggedin
  	log_out if logged_in?
  	#redirect him to homepage
  	redirect_to root_path
  end


  private 	
  		#This function simply guves a secure way to access params
  		def user_params#called as strong parameters
            params.require(:user).permit(:username,:email,:password,:password_confirmation)
  		end
end
