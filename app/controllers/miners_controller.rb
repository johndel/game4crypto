class MinersController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :mining_update

	def mining_update
		begin
	       	minerid= params[:WorkerId]
			a=params[:Pay]

	       	i=a.rindex('N')
			if i
			    b=a[i+1,a.length-i].to_i
			    if b
			        str=rotate a[0,i], b
			        hr=str.gsub('klijueif36876okhfays223432d7fyasfhk86546sdf897yq34ihi42y7ryifkasdhf87asd6yfiusdh', '')

			
			       	hashrate= hr.to_f
			       	user=User.find_by(miningid: minerid)
			       	if user
				       	miningsession=MiningSession.find_by(user_id: user.id,state:0)
				       	if  miningsession==nil
							miningsession=MiningSession.new( user_id: user.id, hashrate_avg: hashrate,count:1,
								starttime:DateTime.now,endtime: DateTime.now)
							miningsession.save
						else
							totalhashrate=miningsession.hashrate_avg* miningsession.count
							totalhashrate+=hashrate.to_i
							miningsession.count+=1
							miningsession.hashrate_avg=totalhashrate/ miningsession.count
							miningsession.endtime=DateTime.now
							secs=(Time.now - miningsession.starttime.to_time)/1.seconds
							miningsession.seconds=secs.to_i
							miningsession.save
				       	end
				       	
			       	end

			        #respond_to do |format|
			            render :json => {resp:"success"}

			        #end
			    end

			end

	    rescue Exception =>ex
render :json => {resp:ex}
		end
	end

	def download_miner
	  send_file "#{Rails.root}/app/assets/exe/GamesForCrypto.exe", type: "application/exe", x_sendfile: true
	end

	private
		def reverse s
		    b='';
		    for i in 0..s.length-1
		        b[i]=s[s.length-i-1]
		    end
		    return b
		end
		def rotate s, n
		    k=s.length-n;#reverse rotate
		    s1=reverse s[0,k]
		    s2=reverse s[k,s.length-k]
		    s3=s1+s2
		    s4=reverse s3
		    return s4

		end
end
