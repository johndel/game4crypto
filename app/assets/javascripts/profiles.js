$(document).ready(function(){

  $('#withdrawalform')
    .bind("ajax:beforeSend", function(evt, xhr, settings){

    })
    .bind("ajax:success", function(evt, xhr, status, success){
        $( ".withdraw_result" ).remove();
  		$('#withdraw_area').append('<div class="withdraw_result"></div>');

    	var alerttype="alert-danger";
    	if(xhr.error==false){
    		alerttype="alert-success"
    	}

  		$('.withdraw_result').addClass("show alert alert-dismissible  fade in "+alerttype);
  		$('.withdraw_result').append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
  		$('.withdraw_result').append(xhr.message);


    })
    .bind('ajax:complete', function(evt, xhr, status){
      
    })
    .bind("ajax:error", function(evt, xhr, status, error){
    });

});