Rails.application.routes.draw do


  root    'users#show'
  
  get 'privacy'       => 'static_pages#privacy'
  get 'terms'             => 'static_pages#terms'
  get 'help'            => 'static_pages#help'
  get 'faq'            => 'static_pages#faq'

  get 'about'           => 'static_pages#about'

  get 'contact'         => 'static_pages#contact'
  
      #UserController
  get 'user_signup'    => 'users#new'
  
  post 'user_login'    => 'users#login'

  delete 'user_logout' => 'users#destroy'

  get 'user_logout' => 'users#destroy'
  
    
#Profiles

  get 'past_games'    => 'profiles#past_games'
  get 'how_it_works'    => 'profiles#how_it_works'
  get 'withdraw'    => 'profiles#withdraw'
  post 'withdraw_dgb'    => 'profiles#withdraw_dgb'
  get 'match_info/:id'    => 'profiles#match_info'


#admins
    post 'admin_login'    => 'admin_users#login'
    get 'admin'    => 'admin_users#new'
    delete 'admin_logout' => 'admin_users#destroy'



    #mining
  get 'mining_start'    => 'miners#mining_update'
  post 'mining_update'    => 'miners#mining_update'
  get 'Games4Crypto', to: "miners#download_miner", as: 'download_miner'


    resources :users  
    resources :admin_users

    get '*unmatched_route', :to => 'application#raise_not_found!'


    match '/auth/:provider/callback', to: 'users#create', via: :all


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
