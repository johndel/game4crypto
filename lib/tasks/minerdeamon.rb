require ::File.expand_path('../../../config/environment', __FILE__)
require ::File.expand_path('../constants', __FILE__)



class DigiByteRPC
  def initialize(service_url)
    @uri = URI.parse(service_url)
  end

  def method_missing(name, *args)
    post_body = { 'method' => name, 'params' => args, 'id' => 'jsonrpc' }.to_json
    resp = JSON.parse( http_post_request(post_body) )
    raise JSONRPCError, resp['error'] if resp['error']
    resp['result']
  end

  def http_post_request(post_body)
    http    = Net::HTTP.new(@uri.host, @uri.port)
    request = Net::HTTP::Post.new(@uri.request_uri)
    request.basic_auth @uri.user, @uri.password
    request.content_type = 'application/json'
    request.body = post_body
    http.request(request).body
  end

  class JSONRPCError < RuntimeError; end
end




def findprofit()
	begin
		#https://whattomine.com/coins/114.json?hr=0.2&p=0.0&fee=0.0&cost=0&hcost=0.0&commit=Calculate
		link="https://whattomine.com/coins/114.json?hr=0.1&p=0.0&fee=0.0&cost=0&hcost=0.0&commit=Calculate"
		url = URI.parse(link)
		req = Net::HTTP::Get.new(url.to_s)
		res = Net::HTTP.start(url.host, url.port) {|http|
		  http.request(req)
		}
		#debugger
		body= res.body
	 	if res.code=="200"
	    	data=JSON.parse body
	    	dollar=data['revenue'];
	    	dollar=dollar[1..dollar.length]#remove dolar sign
	    	dgbcount=data['estimated_rewards']
	    	profit=ProfitRate.new(rate: (dollar.to_f / 24 ), dgbcount: (dgbcount.to_f / 24) ,hashrate:0.1)
	    	profit.save
	    end
	rescue
	end

end

def find_balance 
	begin
		global_info=GlobalInfo.last
		begin
			#find balance
			jsonrpc = DigiByteRPC.new('http://game:Acidance69@127.0.0.1:14022')
			dgb= jsonrpc.getbalance
			global_info.wallet_balance=dgb
		rescue
		end
		#find pool balance
		link="https://digibyte-skein.miningpoolhub.com/index.php?api_key=2ef9e18f299457836a2874bd490e25115f0934a493b65f3c87ebc806ce3d63a8"
		begin
			resp=Faraday.get link
			if resp.success?
				content=JSON.parse(resp.body)
		    	dgb2=content['confirmed_rewards'];
				global_info.pool_balance=dgb2

			end
		rescue
		end
	    global_info.save

	    #if it has been enough time, then create a state object
	    time=Time.now-global_info.created_at
	    if(time.seconds>NEWS_GLOBALINFO_TIME)
	    	info=global_info.dup
	    	info.save
	    end
	rescue
	end

end


def walkthrough
	begin
		offset=0
		limit=20
		loop do
			#we dont want to retrieve  all sessions together. So we retreive n and then loop for those. Then we agai do for n until it is over
			#thats why above loop is needed
			sessions=MiningSession.where(state:0).offset(offset).limit(limit)
			sessions&& sessions.each do |item|

				a=item.endtime.to_datetime
				b= DateTime.now 
				time= b.to_time - a.to_time

				#if he has stopped mining, then close session
				if time> (SESSIONCLOSINGTIME)#it stopped mining 4 mins ago
					item.state=1
					mining_time=item.endtime-item.starttime
					if mining_time.seconds> MININGTIME#we dont do any payout if miningtime is less than 20 mins
						user=User.find_by(id: item.user_id)

						#check if there is a gaming session attached
						if(item.match_id.nil?)
							match=find_match  user,item
							if match
								#debugger
								item.match_id=match.id
								item.save
							end
						end

						profit=ProfitRate.last
						global_info=GlobalInfo.last

						if profit
							#update stats for user
							user.sessions+=1
							user.mom+=(mining_time.to_f/60)
							#calculate hashrate etc
							avg_hashrate=item.hashrate_avg.to_f/(1024*1024)
							mining_time_h=mining_time.to_f/(60*60)
							multiplier=(avg_hashrate/0.1)*mining_time_h
							dgb_profit=profit.dgbcount.to_f * multiplier
							dol_profit=profit.rate.to_f * multiplier

							dgb_profit=dgb_profit*WHATTOMINEERROR#account for error from whattomine

							#adjust for gaming
							game_multiplier=GAMENOPLAYMULTIPLIER
							if(item.match_id)
								#debugger
								match=Match.find_by(id: item.match_id)
								unless match
									match=find_match(user,item) 
								end

								if match
									if user.won?(match)
										game_multiplier=GAMEWINMULTIPLIER
									else
										game_multiplier=GAMELOSSMULTIPLIER
									end
								end
							end
							dgb_profit*=game_multiplier

							#Save to user
							item.digibyte=dgb_profit
							item.profit_rate_id=profit.id
							item.save


							#user.add_credits item.digibyte
							c=user.credits.to_f
							c+=dgb_profit
							total_c=user.total_credits.to_f
							total_c+=dgb_profit

							user.credits=c.to_s
							user.total_credits=total_c.to_s
							user.save

							total_promised=global_info.total_promised.to_f
							total_owed    =global_info.total_owed.to_f
							total_promised+=dgb_profit
							total_owed+=dgb_profit

							global_info.total_promised=total_promised.to_s
							global_info.total_owed=total_owed.to_s
							global_info.save

							#this below line is useless and just  for debug purposes
							#item.digibyte=dgb_profit
							

						end
					else
						item.state=2#invalid
						item.save
						
					end
				else
					#he is still mining
					#if it does not have a dota session attached, see if he is playing dota2
					if(item.match_id.nil?)
						user=User.find_by(id: item.user_id)
						match=find_match  user,item if user
						if match

							item.match_id=match.id
							item.save
						end
					end


				end
			end

			break if sessions.count==0
			offset+=limit
		end
	rescue Exception=> ex
		puts ex
	end
end

def check_running_dota user,mining_session

end
def find_match user,mining_session
    User.readmatches(user.id,10)
	@matches = user.matches.order('started_at DESC').limit(1)
	if @matches && @matches.count==1
		match=@matches[0]#take last match
		#game shue be started after mining started or at least 10 mins before mining
		start_duration=mining_session.starttime- match.started_at
		end_duration=mining_session.endtime- match.started_at

		if(start_duration <600&&#gaming was started 10 min early or later
		  end_duration>0)#gaming was started before miningended
			return match
		end
	end
	return nil
end

#Todo win=1.2 lose=0.6
def apply_game_stats dgbcount
	return dgbcount
end


#This process will just walk through all mining sessiosns and keep updating them
last_profitability_check=DateTime.now
last_balance_check=DateTime.now


findprofit()
find_balance()

loop do
	#sleep(5.minutes)
	profit_lapse=DateTime.now.to_time-last_profitability_check.to_time

	balamce_recheck_lapse=DateTime.now.to_time-last_balance_check.to_time

	#refind profitability multiplier
	if profit_lapse.seconds>(PROFITREADTIME)
		findprofit()
		last_profitability_check=DateTime.now
	end	
	#refind balance 
	if balamce_recheck_lapse.seconds>(PROFITREADTIME)
		find_balance()
		last_balance_check=DateTime.now
	end
	walkthrough
	sleep(SLEEPTIME)
end


